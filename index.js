"use strict";
var jwtRead, jwtWrite, jwtUserConfirmer, supporterCounter;
const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const expressStaticGzip = require("express-static-gzip");
const app = express();
const http = require('http'); 
const https = require('https');
const bodyParser = require('body-parser');
const fs = require('fs-extra');
const axios = require('axios');
const handlebars = require('handlebars');
const locale = require('locale');
const moment = require('moment'); 
const validator = require('validator');
const passwordValidator = require('password-validator');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const cookieParser = require('cookie-parser');
const robots = require('express-robots-txt');
//const auth = require('./auth');
const searchObject = require("search-object");
const payperless = require('./functions'); 

const storeOptions = {
	host: 'localhost',
	port: process.env.STRAPI_DB_PORT,
	user: process.env.STRAPI_DB_USER,
	password: process.env.STRAPI_DB_PASS,
	database: process.env.STRAPI_DB_DB,
	clearExpired: true,
    checkExpirationInterval: 900000,
    expiration: 5270400000,
};   
  
const sessionStore = new MySQLStore(storeOptions);

const options = {
	key: fs.readFileSync(__dirname + '/html/pk/agent2-key.pem'),
	cert: fs.readFileSync(__dirname + '/html/pk/agent2-cert.pem')
};

//app.use(auth);

// setup locale middleware
var supported = ["en", "en_US", "de", "de_DE"];
app.use(locale(supported, "de"));

// setup bodyParser middleware and cookieParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); 
app.use(cookieParser());

// robots
app.use(robots(__dirname + '/robots.txt'));
 
// make resources public available
app.use('/css', express.static(__dirname + '/html/css', {
	enableBrotli: true 
}));
app.use('/js', express.static(__dirname + '/html/js'));
app.use('/img', expressStaticGzip(__dirname + '/html/img')); 
app.use('/fonts', expressStaticGzip(__dirname + '/html/fonts')); 
app.use('/svg', expressStaticGzip(__dirname + '/html/svg'));

//app.use('/pwa', express.static(__dirname + '/html/pwa'));
app.use(express.static(__dirname + "/sw")); // Service Worker Directory

function daysInMilliseconds(days) {
	return 86400000*days;
}

async function loginCardReader() {
	// Login as card reader
	jwtRead = axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: process.env.STRAPI_CARDREADER_MAIL,
		password: process.env.STRAPI_CARDREADER_PASS,
	})
	.then(response => {
		// Handle success
		return response.data.jwt;
	})
	.catch(error => {
		// Handle error.
		console.log('Reader Login error');
		return false;
	});
	return jwtRead;
}

async function loginCardWriter() {
	// Login as card updater
	jwtWrite = axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: process.env.STRAPI_CARDWRITER_MAIL,
		password: process.env.STRAPI_CARDWRITER_PASS,
	})
	.then(response => {
		// Handle success.
		return response.data.jwt;
	})
	.catch(error => {
		// Handle error.
		console.log('Writer Login error');
		return false;
	});
	return jwtWrite;
}

async function loginCardConfirmer() {
	// Login as user updater
	jwtUserConfirmer = axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: process.env.STRAPI_CARDCONFIRMER_MAIL,
		password: process.env.STRAPI_CARDCONFIRMER_PASS,
	})
	.then(response => {
		// Handle success.
		return response.data.jwt;
	})
	.catch(error => {
		// Handle error.
		console.log('Writer Login error');
		return false;
	});
	return jwtUserConfirmer;
}

function updateSupportCounter() {
	supporterCounter = payperless.getSupportersCount().then(resolve => {
		supporterCounter = resolve;
	}).catch(reject => {
		console.log("126", reject);
		supporterCounter = 0;
	});
}

// setup session storage
app.use(session({
	name: 'user_sid',
	secret: process.env.SESSION_STORAGE_SECRET,
	store: sessionStore,
	resave: false,
	saveUninitialized: false, 
	cookie: { 
		secure: true,
		maxAge: daysInMilliseconds(30) 
	}
}));

app.use((req, res, next) => { 
	if(req.cookies.user_sid && !req.session.jwt) {
		res.clearCookie('user_sid');
	}
	next();
});

var sessionChecker = (req, res, next) => {
	if(req.session.jwt && req.cookies.user_sid) {
		if(!payperless.checkJwtExpiration(req)) {
			res.redirect('/barcode');
		} else {
			next();
		}
	} else {
		next();
	}
}

var sessionCheckerBool = (req) => {
	if(req.session.jwt && req.cookies.user_sid) {
		if(!payperless.checkJwtExpiration(req)) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

app.use('/', expressStaticGzip(__dirname + '/public')); 

app.route('/login')
	.get(sessionChecker, (req, res) => {
		let page = fs.readFileSync(__dirname + "/html/pages/login.html", "utf-8", function(err, data) {}); 

		let tags = {
			title: " - Login",
			description: "Payperless - der digitale Kassenbon ✓ jetzt anmelden oder registrieren!"
		};

		let template = handlebars.compile(page);
		page = template({
			header: payperless.getHeader(tags),
			navigation: payperless.getNavigation(req),
			menu: payperless.getSidemenu(req),
			scripts: payperless.getScripts(),
			lang: moment.locale(),
			bodyClass: "login",
			simpleFooter: payperless.getPart('tinyfooter')
		});

		res.send(page);
	})
	.post((req, res) => {

		//console.log("Attempting login", req.body);
		payperless.resloveReCaptcha(req, res).then(cres => {
			if(cres) {
				let email = req.body.email;
				let password = req.body.password;
				axios.post('http://ppcms.payper-less.com:1337/auth/local', {
					identifier: email,
					password: password,
				})
				.then(response => {
					let responseData = {
						email: response.data.user.email,
						confirmed: response.data.user.confirmed
					};
					// Handle success.
					req.session.jwt = response.data.jwt; 

					payperless.getMyUser(response.data.jwt).then(ures => {
						req.session.ppuser = ures;
						payperless.getCardData(ures, jwtRead).then(cres => {
							req.session.ppuser.card = cres;
							responseData.cardnumber = cres[0].cardnumber;
							responseData.cardnumberFull = cres[0].cardnumberfull;
							responseData.cardnumberId = cres[0].id;
	
							req.session.save(error => { 
								if(error) { 
									console.log("255 Couldn't save session to storage", error);
								}	
							});
							res.send({
								redirect: "/barcode",
								uData: responseData
							});
	
						}).catch(rej => {
							console.log("253", rej);
						});
					}).catch(rej => {
						console.log("251:Login myUser error", rej);
						res.redirect('/login');
					});
				})

				.catch(error => {
					console.log("441", error.response.data.message[0].messages);
					res.send({login_error: true, message: error.response.data.message[0].messages[0].id});
				});
			} else {
				res.send({login_error: true, refresh_page: true}); 
			}
		}).catch(rej => {
			console.log("445", rej);
			res.send({login_error: true});
		});
		
	});

app.route('/auth/google/callback')
.post((req, res) => {
	res.redirect('/login');
})
.get((req, res) => {
	let access_token = req.query.access_token;

	axios.get(`http://ppcms.payper-less.com:1337/auth/google/callback?access_token=${access_token}`).then(resolve => {

		req.session.jwt = resolve.data.jwt; 
		req.session.ppuser = resolve.data.user;
		let cardnumber = null;
		let cardnumberFull = null;
		let cardnumberId = null;
		if(resolve.data.user.cards.length !== 0) {
			let digitalCard = resolve.data.user.cards.filter(card => card.digital === true);
			
			if(digitalCard.length > 0) {
				cardnumber = digitalCard[0].cardnumber;
				cardnumberFull = digitalCard[0].cardnumberfull;
				cardnumberId = digitalCard[0].id;
			}
		}

		if(resolve.data.user.cards.length === 0) {
			payperless.generatePPNumber(jwtRead).then(resolve_pp => {
				axios({ 
					method: 'post',
					url: 'http://ppcms.payper-less.com:1337/cards/',
					headers: {
						Authorization: `Bearer ${jwtWrite}`,
					},
					data: {
						cardnumber: resolve_pp.substring(3,12),
						usercontactmail: resolve.data.user.email,
						emailconfirmed: true,
						user: [resolve.data.user.id],
						cardnumberfull: resolve_pp,
						digital: true
					}
				}).then(resolve => {
					let card = resolve.data;
					delete card.user;
					if(!req.session.ppuser.cards.length) {
						req.session.ppuser.cards.push(card);
						req.session.save(err => {
							if(err) {
								console.log("346 Couldn't save session", err);
							}
						});
					} else {
						if(!searchObject(req.session.ppuser.cards, card.id)) {
							req.session.ppuser.cards.push(card);
							req.session.save(err => {
								if(err) {
									console.log("356 Couldn't save session", err);
								}
							});
						}
					}
					res.redirect('/receipts?cf=' + resolve_pp + "&c=" + resolve_pp.substring(3,12) + "&id=" + card.id);
				}).catch(rej => {
					console.log("391", rej);
					res.redirect('/login?error=providererror');
				})
			}).catch(rej => {
				console.log("395", rej);
				res.send({registration_error: true});
			});
		} else if(resolve.data.user.cards.length > 0) {
			let hasDigital = false;
			for(let card of resolve.data.user.cards) {
				if(card.digital) {
					hasDigital = true
				}
			}
			if(!hasDigital) {
				payperless.generatePPNumber(jwtRead).then(resolve_pp => {
					axios({ 
						method: 'post',
						url: 'http://ppcms.payper-less.com:1337/cards/',
						headers: {
							Authorization: `Bearer ${jwtWrite}`,
						},
						data: {
							cardnumber: resolve_pp.substring(3,12),
							usercontactmail: resolve.data.user.email,
							emailconfirmed: true,
							user: [resolve.data.user.id],
							cardnumberfull: resolve_pp,
							digital: true
						}
					}).then(resolve => {
						res.redirect('/receipts?cf=' + cardnumberFull + "&c=" + cardnumber + "&id=" + cardnumberId);
					}).catch(rej => {
						console.log("391", rej);
						res.redirect('/login?error=providererror');
					})
				}).catch(rej => {
					console.log("395", rej);
					res.redirect('/login?error=providererror');
				});
			} else {
				res.redirect('/receipts?cf=' + cardnumberFull + "&c=" + cardnumber + "&id=" + cardnumberId);
			}
		} else {
			res.redirect('/receipts?cf=' + cardnumberFull + "&c=" + cardnumber + "&id=" + cardnumberId);
		}
	}).catch(reject => {
		res.redirect('/login?provider_error=1');
		console.log("323", reject);
	});
}); 
	
app.get('/register', (req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/register.html", "utf-8", function(err, data) {}); 
	let usermail = "";
	if(req.query.email) {
		usermail = req.query.email;
	}

	let tags = {
		title: " - Registration",
		description: "Payperless - der digitale Kassenbon ✓ Jetzt registrieren und mitmachen!"
	};

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(),
		lang: moment.locale(),
		bodyClass: "register",
		simpleFooter: payperless.getPart('tinyfooter'),
		email: usermail
	});

	res.send(page);
});

app.post('/register', (req, res) => { // TODO Register testing
	let usermail = req.body.email;
	let password = req.body.password;
	let token = req.body.token;
	let passwordSchema = new passwordValidator();
	let errorReport = {};
	let responseObj = {};

	passwordSchema
		.is().min(8)
		.is().max(100)
		.has().uppercase()
		.has().lowercase()
		.has().digits()
		.has().not().spaces();

		payperless.resloveReCaptcha(req, res).then(val => {
		if(val) {

			if(validator.isEmail(usermail) && passwordSchema.validate(password)) {

				payperless.checkIfEmailAlreadyUsed(usermail, jwtRead).then(val => { 
					let alreadyHasConfirmedEmail = false;
					let regObj = {
						username: usermail,
						email: usermail,
						password: password,
						confirmed: false
					};
					if(val) {
						regObj.cards = [val.id];
						alreadyHasConfirmedEmail = val.emailconfirmed;
					}

					let part = fs.readFileSync(__dirname + "/html/pages/parts/registersuccess.html", "utf-8", function(err, data) {}); 

					let template = handlebars.compile(part);
					part = template({
						simpleFooter: payperless.getPart('tinyfooter'),
						customerEmail: usermail
					}); 

					axios({
						url: "http://ppcms.payper-less.com:1337/auth/local/register",
						method: "post",
						data: regObj,
					})
						.then(val => {
						let userJwt = val.data.jwt;
						let newUserData = val.data.user;

						if(!alreadyHasConfirmedEmail) {
							axios({
								method: 'put', 
								url: 'http://ppcms.payper-less.com:1337/users/' + newUserData.id,
								headers: {
									Authorization: `Bearer ${jwtUserConfirmer}`,
								},
								data: { 
									"confirmed": false
								}
							})
							.then(response => {
								payperless.sendUserActivationLink(newUserData.id, usermail).then(response => {
									payperless.generatePPNumber(jwtRead).then(resolve => {
										axios({ 
											method: 'post',
											url: 'http://ppcms.payper-less.com:1337/cards/',
											headers: {
												Authorization: `Bearer ${jwtWrite}`,
											},
											data: {
												cardnumber: resolve.substring(3,12),
												usercontactmail: usermail,
												emailconfirmed: alreadyHasConfirmedEmail,
												user: [newUserData.id],
												cardnumberfull: resolve,
												digital: true
											}
										}).then(resolve => {
											res.send(part);
										}).catch(rej => {
											console.log("391", rej);
											res.send({registration_error: true});
										})
										
									}).catch(rej => {
										console.log("395", rej);
										res.send({registration_error: true});
									});
									
								}).catch(rej => {
									console.log("398", rej);
									res.send({cantsendconfirmation_error: true}); 
								});
							})
							.catch(rej => {
								console.log('405', rej);
							});
						} else {
							res.send(part);
						}	
					})
					.catch(rej => {
						console.log('374 Registrationerror', rej[0].data);
						console.log(regObj);
						res.send({registration_error: true});
					});
		
				}).catch(rej => {
					console.log("379", rej.data.message);
				});
			 
			} else {
				if(!passwordSchema.validate(password)) {
					errorReport.password_error = true;
				}
				if(!validator.isEmail(usermail)) {
					errorReport.email_error = true;
				}
				res.send(errorReport);
			}

		} else {
			console.log("trust_error" ,val);
			res.send({trust_error: true});
		}
	}).catch(rej => {
		console.log("356", rej);
	});

	
});

app.get('/registercard', (req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/registercard.html", "utf-8", function(err, data) {}); 

	let tags = {
		title: " - Karte registrieren",
		description: "Payperless - der digitale Kassenbon ✓ Jetzt Ihre Karte registrieren und mitmachen!"
	};

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(),
		lang: moment.locale(),
		bodyClass: "cardregister",
		simpleFooter: payperless.getPart('tinyfooter')
	});

	res.send(page);
});

app.post('/begincardreg', (req, res) => {
	moment.locale(req.locale.substr(0,2));
	
	let cardnumber = req.body.cardnumber;
	let strapiCardObj = null;

	payperless.resloveReCaptcha(req, res).then(val => {
		if(val) {

			payperless.getCardInfosByNumber(cardnumber, jwtRead).then((val) => {

				if(val && typeof val === 'object') {
					if('usercontactmail' in val && (val.usercontactmail === null || val.usercontactmail === '')) {
						axios.get('http://ppcms.payper-less.com:1337/cards?cardnumber=' + cardnumber, {
							headers: {
								Authorization: `Bearer ${jwtRead}`,
							}
						})
						.then(response => {
							strapiCardObj = response.data[0];
			
							if(typeof strapiCardObj === 'object' && strapiCardObj !== "undefined" && strapiCardObj !== null) {
								if(Object.keys(strapiCardObj).length > 0) {
									let cardText = "";
									if(moment.locale() == "de") {
										cardText = "Ihre Kartennummer: ";
									} else {
										cardText = "Your cardnumber: ";
									}
									let part = payperless.getPart("cardreg", {
										cardnumber: cardnumber,
										yourCardnumber: cardText,
										simpleFooter: payperless.getPart('tinyfooter')
									});
									if(part) {
										res.send(part);
									} else {
										res.json({part_error: true});
									}
								} else {
									res.json({notfound_error: true});
								}
							} else {
								res.json({notfound_error: true});
							}
						}).catch(error => {
							console.log("Cardinfo error");
							res.json({notfound_error: true});
						});
					} else {
						/* Allow card to be registered by the default formular only once*/
						/* Create a formular that allows the user to create an account with the already to the card assigned email address*/
						let cardText = "";
						if(moment.locale() == "de") {
							cardText = "Ihre Kartennummer: ";
						} else {
							cardText = "Your cardnumber: ";
						}
						let part = payperless.getPart("alreadyreg", {
							cardnumber: cardnumber,
							yourCardnumber: cardText,
							simpleFooter: payperless.getPart('tinyfooter')
						});
					
						if(part) {
							res.send(part);
						} else {
							res.send({part_error: true});
						}	
					}
				} else {
					res.json({notfound_error: true});
				}
		
			}).catch(rej => {
				console.log("406", rej);
			});

		} else {
			res.send({trust_error: true});
		}
	}).catch(rej => {
		console.log('427', rej);
		res.send({trust_error: true});
	});
});

app.post('/successcardreg', (req, res) => { 
	moment.locale(req.locale.substr(0,2));
	let cardnumber = req.body.cardnumber;
	let email = req.body.umail;

	let cardText = "";
	if(moment.locale() == "de") {
		cardText = "Ihre Kartennummer: ";
	} else {
		cardText = "Your cardnumber: ";
	} 

	if(validator.isEmail(email)) {

		payperless.checkIfEmailAlreadyUsed(email, jwtRead).then((val) => {

			if(!val) { 
				payperless.sendActivationLink(email, cardnumber).then(val => {

					let part = payperless.getPart("cardregsuccess", {
						cardnumber: cardnumber,
						yourCardnumber: cardText,
						customerEmail: email,
						simpleFooter: payperless.getPart('tinyfooter')
					});
				
					if(part) {
						res.send(part);
					} else {
						res.send({part_error: true});
					}

				}).catch(error => { console.log("584", error); });
			} else {
				/* Email ist already in use. Create formular to tell user that email is already assigned to a card */
				/* User has to create an account with the used email address to be able to remove the connection to the assigned card */
				let cardText = "";
				if(moment.locale() == "de") {
					cardText = "Ihre Kartennummer: ";
				} else {
					cardText = "Your cardnumber: ";
				}
				let part = payperless.getPart("emailinuse", {
					cardnumber: cardnumber,
					yourCardnumber: cardText,
					simpleFooter: payperless.getPart('tinyfooter'),
					userEmail: email 
				});
			
				if(part) {
					res.send(part);
				} else {
					res.send({part_error: true});
				}	
			}
		}).catch(rej => {
			console.log("491", rej);
		});
		
	} else {
		res.send({email_error: true});
	}
});

app.route('/forgot')
.post((req, res) => {
	if(sessionCheckerBool(req)) {
		payperless.resetPassword(req.session.ppuser.email);
	}
})
.get((req, res) => {
	if(sessionCheckerBool(req)) {
		let page = fs.readFileSync(__dirname + "/html/pages/emailforgot.html", "utf-8", function(err, data) {}); 

		let tags = { 
			title: " - Passwort ändern",
			description: "Payperless - der digitale Kassenbon ✓"
		};

		let template = handlebars.compile(page);
		page = template({
			header: payperless.getHeader(tags),
			navigation: payperless.getNavigation(req),
			menu: payperless.getSidemenu(req),
			scripts: payperless.getScripts(),
			lang: moment.locale(),
			bodyClass: "forgot",
			simpleFooter: payperless.getPart('tinyfooter')
		});
		res.send(page);
	}
});
 
app.get('/reset', (req, res) => {
	req.session.ppuser.resetcode = req.query.code;
	if(sessionCheckerBool(req)) {
		if('code' in req.query) {
			let page = fs.readFileSync(__dirname + "/html/pages/reset.html", "utf-8", function(err, data) {}); 

			let tags = { 
				title: " - Passwort ändern",
				description: "Payperless - der digitale Kassenbon ✓"
			};

			let template = handlebars.compile(page);
			page = template({
				header: payperless.getHeader(tags),
				navigation: payperless.getNavigation(req),
				menu: payperless.getSidemenu(req),
				scripts: payperless.getScripts(),
				lang: moment.locale(),
				bodyClass: "reset",
				simpleFooter: payperless.getPart('tinyfooter')
			});

			res.send(page);
		} else {
			let page = fs.readFileSync(__dirname + "/html/pages/error.html", "utf-8", function(err, data) {}); 

			let tags = { 
				title: " - Fehler bei Zurücksetzen des Passworts",
				description: "Payperless - der digitale Kassenbon ✓"
			};

			let template = handlebars.compile(page);
			page = template({
				header: payperless.getHeader(tags),
				navigation: payperless.getNavigation(req),
				menu: payperless.getSidemenu(req),
				scripts: payperless.getScripts(),
				lang: moment.locale(),
				bodyClass: "error",
				simpleFooter: payperless.getPart('tinyfooter')
			});

			res.send(page);
		}
	}
});

app.post('/reset', (req, res) => {
	if(sessionCheckerBool(req)) {
		let code = req.session.ppuser.resetcode;
		let passwordSchema = new passwordValidator();
		let password = req.body.password;
		let passwordRepeat = req.body.passwordrepeat;
		passwordSchema
			.is().min(8)
			.is().max(100)
			.has().uppercase()
			.has().lowercase()
			.has().digits()
			.has().not().spaces();
		if(password === passwordRepeat) {
			if(passwordSchema.validate(password)) {
				axios({
					method: "post",
					url: "http://ppcms.payper-less.com:1337/auth/reset-password",
					data: {
						code: code,
						password: password,
						passwordConfirmation: password	
					}
				})
				.then(response => {
					res.send({success: true});
				})
				.catch(error => {
					res.send({passwordchangeerror: true});
				}); 
			} else {
				res.send({passwordcomplexityerror: true});
			}
		} else {
			res.send({identicerror: true});
		}
	}
});

app.route('/linkcard')
	.get((req, res) => {
		if(sessionCheckerBool(req)) {
			let page = fs.readFileSync(__dirname + "/html/pages/linkcard.html", "utf-8", function(err, data) {}); 

			let tags = { 
				title: " - Karte verknüpfen",
				description: "Payperless - der digitale Kassenbon ✓"
			};

			let template = handlebars.compile(page);
			page = template({
				header: payperless.getHeader(tags),
				navigation: payperless.getNavigation(req),
				menu: payperless.getSidemenu(req),
				scripts: payperless.getScripts(),
				lang: moment.locale(),
				bodyClass: "linkcard",
				simpleFooter: payperless.getPart('tinyfooter')
			});

			res.send(page);
		}
	})
	.post ((req, res) => {
		if(sessionCheckerBool(req)) {
			//console.log(req.body);
			let cardnumber = req.body.cardnumber;
			let email = req.session.ppuser.email;
			let confirmed = req.session.ppuser.confirmed;
			let userId = req.session.ppuser.id;
			payperless.getCardInfosByNumber(cardnumber, jwtRead).then(resolve => {
				if(!resolve) {
					res.send({notfound_error: true});
				} else {
					let cardId = resolve.id;
					let cardmail = resolve.usercontactmail;
					if(cardmail === null || cardmail === "") {
						axios({
							method: 'put',
							url: 'http://ppcms.payper-less.com:1337/cards/' + cardId,
							headers: {
								Authorization: `Bearer ${jwtWrite}`,
							},
							data: {
								cardnumber: cardnumber,
								usercontactmail: email,
								emailconfirmed: confirmed,
								user: [userId]
							}
						}).then(resolve => {
							res.send({success: true});
						}).catch(rej => {
							console.log("669", rej);
						});
						
					} else {
						res.send({already_used: true});
					}
					
				}
				
			}).catch(rej => {
				console.log("652", rej);
			});
			
		}
	});

app.route('/api/billingtest')
.get((req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/billingtest.html", "utf-8", function(err, data) {}); 
	res.send(page);
});

app.route('/api/login')
.post((req, res) => { 
	axios.post('http://ppcms.payper-less.com:1337/auth/local', {
		identifier: req.body.email,
		password: req.body.password,
	})
	.then(response => {
		res.send({token: response.data.jwt, ownerId: response.data.user.id});
	}).catch(reject => {
		res.send({login_error: true, error: reject});
		console.log("731", reject);
	});
});

app.route('/api/tokencheck')
.post((req, res) => {
	payperless.getMyUser(req.body.token).then(resolve => {
		console.log("709", resolve);
		if(resolve) {
			res.send({valid: true});
		} else {
			res.send({valid: false});
		}
	}).catch(reject => {
		res.send({valid: false, error: reject});
		console.log("711", reject);
	})
});

app.route('/api/billings')
.post((req, res) => {
	if('bill' in req.body) {
		let ecObj = { // Kartenzahlung
			iban: null,
			vuNumber: null,
			terminalID: null,
			emvAID: null,
			posInfo: null,
			asTime: null,
			amount: null
		};
	
		if('ec' in req.body) { // { iban: DE07123412341234123412, vuNumber: "", terminalID: "" }
			for(let ecProp in req.body.ec) { // only overwrite existing key data
				ecObj[ecProp] = req.body.ec[ecProp];
			}
		} else {
			ecObj = null;
		}
	
		let concessionaireObj = [ // Konzessionäre z.B. Sushi Cicle im REWE
			{
				GID: null,
				shopAdress: {
					shopStreet: "", // "Bahnhofplatz 1"
					shopZip: null, // 80335
					shopLocation: null // Butzbach
				},
				shopTaxID: null, // 163 146 0050
				label: null
			}
		];	
	
		if('concessionaire' in req.body) { // { concessionaire: [ { GID: "xxx", shopAdress: {}, ... }, {} ] } multiple concessionaires possible
			concessionaireObj = req.body.concessionaire;
		} else {
			concessionaireObj = null;
		}
		
		let billingObj = {
			PPID: null, // 962724136
			GID: null, // Händler ID in unserer Datenbank
			shopAdress: {
				shopStreet: "", // "Bahnhofplatz 1"
				shopZip: null, // 80335
				shopLocation: null // Butzbach
			},
			shopUIDNr: null, // "DE815225188 (Umsatzsteuer ID)"
			shopTaxID: null, // 163 146 0050
			billNumber: null, // 3046
			traceNr: null, // 255831
			shopNumber: null, // 5759
			checkoutCounter: null, // 3 Kassennummer
			cashierNumber: null, // 373737 // Kassierernummer
			billDate: null, // Timestamp
			items: [], // [ {description: "Spruesahne 30%", quantity: 2, vatType: "B", piecePrice: 0.99}, {description: "Vanille", quantity: 1, vatType: "B", piecePrice: 1.99} ]
			ec: null, // EC-Karte
			concessionaire: null, // konzessionär 
			aRates:	19.0, // current A rates in Germany
			bRates: 7.0, // current B rates in Germany
			typeATotalNet: 0, // 1.34
			typeBTotalNet: 0, // 1.59
			typeATotalGross: 0, // 4.64
			typeBTotalGross: 0,  // 4.96
			totalNet: 0, // 5.98
			totalGross: 0, // 6.55
			additionalCustomInfos: null, // any html <p><strong>Get PAYBACK now!<strong></p>
			token: null // security token | some way to make the bill a legal element 
		};

		let token = req.body.shoptoken;
	
		if(ecObj !== null) {
			billingObj.ec = ecObj;
		}
		
		if(concessionaireObj !== null) {
			billingObj.concessionaire = concessionaireObj;
		}

		for(let billProp in req.body.bill) {
			if(billProp != "concessionaire" && billProp != "ec") {
				billingObj[billProp] = req.body.bill[billProp];
			}
		}

		axios({
			method: 'post',
			url: 'http://ppcms.payper-less.com:1337/billings',
			headers: {
				Authorization: `Bearer ${req.body.token}`,
			},
			data: {
				billinginfo: billingObj,
				cardnumber: req.body.cardnumber,
				owningShop: req.body.ownerId
			}
		}).then(resolve => {
			res.send({code: 200, sucess: true});
		}).catch(reject => {
			res.send({createbill_error: true, error: reject});
		});

	} else {
		res.send({
			code: 400,
			message: "'bill' key is missing in request body (example: { bill: {...} })"
		});
	}
})
.get((req, res) => {

});

app.get('/activate', (req, res) => {
	moment.locale(req.locale.substr(0,2));

	if(payperless.decryptValue(req.query.m) && payperless.decryptValue(req.query.c)) {
		let mparam = payperless.decryptValue(req.query.m);	// email
		let cparam = payperless.decryptValue(req.query.c);	// cardumber

		let activateErrorPage = fs.readFileSync(__dirname + "/html/pages/emailactivationerror.html", "utf-8", function(err, data) {}); 

		let tags = {
			title: " - Aktivierung fehlgeschlagen",
			description: "Payperless - der digitale Kassenbon ✓"
		};

		let template = handlebars.compile(activateErrorPage);
		activateErrorPage = template({
			header: payperless.getHeader(tags),
			navigation: payperless.getNavigation(req),
			menu: payperless.getSidemenu(req),
			lang: moment.locale(),
			bodyClass: "activationerror",
			simpleFooter: payperless.getPart('tinyfooter'),
			cardnumber: req.query.c,
			useremail: mparam
		});
	
		payperless.getCardInfosByNumber(cparam, jwtRead).then(val => {

			if(typeof val === 'object') {

				axios({
					method: 'put', 
					url: 'http://ppcms.payper-less.com:1337/cards/' + val.id,
					headers: {
						Authorization: `Bearer ${jwtWrite}`,
					},
					data: { 
						"usercontactmail": mparam,
						"emailconfirmed": true 
					}
				})
				.then((response) => {
					if(response.status == 200) {	
	
						let activateSuccessPage = fs.readFileSync(__dirname + "/html/pages/emailactivationsuccess.html", "utf-8", function(err, data) {}); 
	
						let tags = {
							title: " - Aktivierung erfolgreich",
							description: "Payperless - der digitale Kassenbon ✓"
						};

						let template = handlebars.compile(activateSuccessPage);
						activateSuccessPage = template({
							header: payperless.getHeader(tags),
							navigation: payperless.getNavigation(req),
							menu: payperless.getSidemenu(req),
							lang: moment.locale(),
							bodyClass: "acivationsuccess",
							simpleFooter: payperless.getPart('tinyfooter'),
							useremail: mparam
						});	
						res.send(activateSuccessPage);
					} else {
						res.send(activateErrorPage);
					}
				})
				.catch(error => {
					res.send(activateErrorPage);
				});

			} else {
				res.send(activateErrorPage);
			}
		
		}).catch(rej => {
			res.send(activateErrorPage);
		});
	} else {
		res.send(activateErrorPage);
	}
});

app.get('/useractivate', (req, res) => {
	moment.locale(req.locale.substr(0,2));

	if(payperless.decryptValue(req.query.m) && payperless.decryptValue(req.query.c)) {
		let mparam = payperless.decryptValue(req.query.m);	// id
		let cparam = payperless.decryptValue(req.query.c);	// email

		let activateErrorPage = fs.readFileSync(__dirname + "/html/pages/useremailactivationerror.html", "utf-8", function(err, data) {}); 

		let tags = {
			title: " - Aktivierung fehlgeschlagen",
			description: "Payperless - der digitale Kassenbon ✓"
		};

		let template = handlebars.compile(activateErrorPage);
		activateErrorPage = template({
			header: payperless.getHeader(tags),
			navigation: payperless.getNavigation(req),
			menu: payperless.getSidemenu(req),
			lang: moment.locale(),
			bodyClass: "activationerror",
			simpleFooter: payperless.getPart('tinyfooter'),
			id: mparam,
			useremail: cparam
		});
	
		axios({
			method: 'put', 
			url: 'http://ppcms.payper-less.com:1337/users/' + mparam,
			headers: {
				Authorization: `Bearer ${jwtUserConfirmer}`,
			},
			data: { 
				"confirmed": true 
			}
		})
		.then((response) => {
			if(response.status == 200) {	

				let activateSuccessPage = fs.readFileSync(__dirname + "/html/pages/useremailactivationsuccess.html", "utf-8", function(err, data) {}); 

				let tags = {
					title: " - Aktivierung erfolgreich",
					description: "Payperless - der digitale Kassenbon ✓"
				};

				let template = handlebars.compile(activateSuccessPage);
				activateSuccessPage = template({
					header: payperless.getHeader(tags),
					menu: payperless.getSidemenu(req),
					navigation: payperless.getNavigation(req),
					lang: moment.locale(),
					bodyClass: "acivationsuccess",
					simpleFooter: payperless.getPart('tinyfooter'),
					useremail: cparam
				});	
				res.send(activateSuccessPage);
			} else {
				res.send(activateErrorPage);
			}
		})
		.catch(error => {
			res.send(activateErrorPage);
		});
	} else {
		res.send(activateErrorPage);
	}
});

app.get('/success', (req, res) => {
	moment.locale(req.locale.substr(0,2));

	let page = fs.readFileSync(__dirname + "/html/pages/success.html", "utf-8", function(err, data) {}); 

	let tags = {
		title: " - Bestätigung erfolgreich",
		description: "Payperless - der digitale Kassenbon ✓"
	};

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(["jquery.min", "custom", "bootstrap"]), 
		lang: moment.locale(),
		bodyClass: "activationsuccess",
		simpleFooter: payperless.getPart('tinyfooter') 
	});
 
	res.send(page);
});

app.get('/retry', (req, res) => { // on email acivation link error | resend activation link to email
	let cardnumber = payperless.decryptValue(req.query.c);
});

app.get('/barcode', (req, res) => {

	//if(req.session.ppuser.email == "ealy.markus@gmail.com") {
		if(!payperless.isPhone(req.headers['user-agent'])) {
			res.redirect('/receipts');
		} else {
			if(sessionCheckerBool(req)) {
				let userObj = req.session.ppuser;
				let digitalCard = userObj.cards.filter(card => card.digital === true);
				console.log(userObj, digitalCard);
				let cardnumber = digitalCard.cardnumber;
				let page = fs.readFileSync(__dirname + "/html/pages/barcode.html", "utf-8", function(err, data) {}); 
		
				let tags = { 
					title: " - Mein Barcode",
					description: "Payperless - der digitale Kassenbon ✓"
				};
		
				let template = handlebars.compile(page);
				page = template({
					header: payperless.getHeader(tags),
					navigation: payperless.getNavigation(req),
					menu: payperless.getSidemenu(req),
					scripts: payperless.getScripts(),
					lang: moment.locale(),
					bodyClass: "barcode",
					simpleFooter: payperless.getPart('tinyfooter')
				});
		
				res.send(page);
			} else {
				res.redirect('/login');
			}
		}
	//} else {
	//	payperless.underConstruction(res, req);
	//}
});

app.get('/receipts', (req, res) => {
	if(req.session.ppuser.email == "ealy.markus@gmail.com") {
		if(sessionCheckerBool(req)) {
			let userObj = req.session.ppuser;
			let userCards = [];
			if('card' in userObj) {
				let userCard = userObj.card[0];
				for(let card of userObj.card) {
					userCards.push(card.cardnumber);
				}
			}
			let page = fs.readFileSync(__dirname + "/html/pages/receipts.html", "utf-8", function(err, data) {}); 

			let tags = {
				title: " - Meine Kassenbons",
				description: "Payperless - der digitale Kassenbon ✓"
			}

			let template = handlebars.compile(page);
			page = template({
				header: payperless.getHeader(tags),
				navigation: payperless.getNavigation(req),
				menu: payperless.getSidemenu(req),
				scripts: payperless.getScripts(),
				lang: moment.locale(),
				bodyClass: "receipt",
				simpleFooter: payperless.getPart('tinyfooter')
			});

			res.send(page);
		} else {
			res.redirect('/login');
		}
	} else {
		payperless.underConstruction(res, req);
	}
});

app.route('/profile')
	.get((req, res) => {
		//if(req.session.ppuser.email == "ealy.markus@gmail.com") { 
			let page = fs.readFileSync(__dirname + "/html/pages/profile.html", "utf-8", function(err, data) {}); 

			let tags = {
				title: " - Mein Profil",
				description: "Payperless - der digitale Kassenbon ✓"
			}

			let hasProvider = req.session.ppuser.provider;
			let providerDiv = "";

			if(hasProvider !== "local") {
				providerDiv = "<i id='hasProvider'>" + hasProvider + "</i>";
			}

			let template = handlebars.compile(page);
			page = template({
				header: payperless.getHeader(tags),
				navigation: payperless.getNavigation(req),
				menu: payperless.getSidemenu(req),
				scripts: payperless.getScripts(),
				lang: moment.locale(),
				bodyClass: "profile",
				simpleFooter: payperless.getPart('tinyfooter'),
				currentEmail: req.session.ppuser.email,
				provider: providerDiv
			});

			res.send(page);
		//} else {
		//	payperless.underConstruction(res, req);
		//}
	})
	.post((req, res) => {

	});

	app.post('/changemail', (req, res) => {
	
		payperless.resloveReCaptcha(req, res).then(cres => {
			if(cres) {
				let password = req.body.password;
				let newEmail = req.body.email;

				let user = {
					identifier: req.session.ppuser.email,
					password: password,
				};

				payperless.checkUserCredentials(user)
				.then(resolve => {
					if(resolve) { 
						let oldMail = req.session.ppuser.email;
						payperless.changeUserEmail(req.session.ppuser.email, newEmail).then(resolve => {
							req.session.ppuser.username = newEmail;
							req.session.ppuser.email = newEmail;
							for(let card of req.session.ppuser.card) {
								card.usercontactmail = newEmail;
							}
							payperless.changeCardContactmail(oldMail, newEmail);
							req.session.save(function(err) {
								if(err) {
									console.log("1070", err);
								}
								//console.log(req.session);
							});
							res.send({emailchange_error: false});
							//console.log("1060", resolve);
						}).catch(reject => {
							res.send({emailchange_error: true});
							console.log("1062", reject);
						});
					} else {
						
					}
				}).catch(reject => {

				});
			}
		}).catch(reject => {
			res.send({emailchange_error: true});
		});
	});

	app.get('/mailchange', (req, res) => {

	});

app.get('/search', (req, res) => {
	let searchObj = req.query;
	let cardNumber = req.session.ppuser.card[0].cardnumber;
	let billFile = fs.readJson(__dirname + "/bills/" + cardNumber + ".json").then(billData => {
		let filterResult = billData.filter(bill => {
			return searchObject(bill, searchObj.search);
		});
		console.log(JSON.stringify(filterResult));
	}).catch(reject => {
		console.log("1106", reject);
	});
});

app.get('/privacy', (req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/privacy.html", "utf-8", function(err, data) {}); 

	let tags = {
		title: " - Datenschutz",
		description: "Payperless - der digitale Kassenbon ✓"
	}

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(),
		lang: moment.locale(),
		bodyClass: "privacy",
		simpleFooter: payperless.getPart('tinyfooter')
	});

	res.send(page);
});

app.get('/startprivacy', (req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/startprivacy.html", "utf-8", function(err, data) {}); 

	let tags = {
		title: " - Datenschutz",
		description: "Payperless - der digitale Kassenbon ✓"
	}

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(),
		lang: moment.locale(),
		bodyClass: "privacy",
		simpleFooter: payperless.getPart('tinyfooter')
	});

	res.send(page);
});

app.route('/contact')
	.get((req, res) => {
		let page = fs.readFileSync(__dirname + "/html/pages/contact.html", "utf-8", function(err, data) {}); 

		let tags = {
			title: " - Kontakt",
			description: "Payperless - der digitale Kassenbon ✓"
		}

		let template = handlebars.compile(page);
		page = template({
			header: payperless.getHeader(tags),
			navigation: payperless.getNavigation(req),
			menu: payperless.getSidemenu(req),
			scripts: payperless.getScripts(),
			lang: moment.locale(),
			bodyClass: "contact",
			simpleFooter: payperless.getPart('tinyfooter')
		});

		res.send(page);
	})
	.post((req, res) => {
		let contactObj = req.body;
		payperless.sendContactMail(contactObj, req, res);
	});

app.route('/supporter') 
	.post((req, res) => {
		payperless.addSupporter(req, res)
	})
	.get((req, res) => {
		res.send({count: supporterCounter});
	});


app.get('/logout', (req, res) => {
	req.session.destroy(err => {
		if(err) {
			console.log("862 logout error ", err);
			res.redirect('/');
		} else {
			res.redirect('/');
		} 
	})
});

app.route('/') 
	.get((req, res) => {
		if(req.protocol === 'http') {
			res.redirect('https://payper-less.com/'); 
		} else  {
			moment.locale(req.locale.substr(0,2));

			let page = fs.readFileSync(__dirname + "/public/index.html", "utf-8", function(err, data) {}); 
			
			res.send(page);
		}
	});

	
app.route('/ppadmin') 
	.get((req, res) => {
		if(req.protocol === 'http') {
			res.redirect('https://payper-less.com/'); 
		} else  {
			moment.locale(req.locale.substr(0,2));

			let page = fs.readFileSync(__dirname + "/html/pages/index.html", "utf-8", function(err, data) {}); 
			
			let tags = {
				title: " - Digitaler Kassenbon",
				description: "Payperless - der digitale Kassenbon ✓ gegen den Papierwahn durch die Kassenbonpflicht"
			};

			let template = handlebars.compile(page);
			page = template({
				header: payperless.getHeader(tags),
				navigation: payperless.getNavigation(req),
				menu: payperless.getSidemenu(req),
				scripts: payperless.getScripts(["jquery.min", "custom", "bootstrap"]), 
				lang: moment.locale(),
				bodyClass: "home",
				footer: payperless.getPart('footer')
			});
		
			res.send(page);
		}
	});

/* PWA Route handling */
/*app.get('/pwa', (req, res) => {
	if(req.protocol === 'http') { 
		res.redirect('https://payper-less.com/');
	} else  {
		res.sendFile(__dirname + '/public/index.html');
	} 
});

app.get('/pwa/register', (req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/register.html", "utf-8", function(err, data) {}); 
	let usermail = "";
	if(req.query.email) {
		usermail = req.query.email;
	}

	let tags = {
		title: " - Registrieren",
		description: "Payperless - der digitale Kassenbon ✓ Jetzt registrieren und mitmachen!"
	};

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(),
		lang: moment.locale(),
		bodyClass: "register",
		simpleFooter: payperless.getPart('tinyfooter'),
		email: usermail
	});

	res.send(page);
});

app.get('/pwa/registercard', (req, res) => {
	let page = fs.readFileSync(__dirname + "/html/pages/registercard.html", "utf-8", function(err, data) {}); 

	let tags = {
		title: " - Aktivierung fehlgeschlagen",
		description: "Payperless - der digitale Kassenbon ✓ Jetzt Ihre Karte registrieren und mitmachen!"
	};

	let template = handlebars.compile(page);
	page = template({
		header: payperless.getHeader(tags),
		navigation: payperless.getNavigation(req),
		menu: payperless.getSidemenu(req),
		scripts: payperless.getScripts(),
		lang: moment.locale(),
		bodyClass: "cardregister",
		simpleFooter: payperless.getPart('tinyfooter')
	});

	res.send(page);
});*/
/* ------------------ */

const port = process.env.PORT || 9052;  
 
var httpsServer = https.createServer(options, app)
	.listen(port, () => {
		console.log(`Payperless Server listening on port :${port}.`);
		setInterval(() => {
			updateSupportCounter();
		}, 120000);
		loginCardReader().then((val) => {
			console.log("Reader ready"); 
			jwtRead = val;
		}).catch((rej) => {  
			console.log("Reader Login Error 1131", rej);
		}); 
		loginCardWriter().then((val) => {
			console.log("Writer ready");   
			jwtWrite = val;
		}).catch((rej) => {
			console.log("Writer Login Error 1137", rej);
		});
		loginCardConfirmer().then((val) => {
			console.log("cardConfirmer ready");
			jwtUserConfirmer = val;
		}).catch(rej => {
			console.log("Confirmer Login Error 1143", rej);
		});
		updateSupportCounter();
		setInterval(() => { loginCardReader().then((val) => { jwtRead = val; }).catch(rej => { console.log("Reader Login Error", rej); return false; }); loginCardWriter().then(val => { jwtWrite = val; }).catch(rej => { console.log(rej); return false; }); loginCardConfirmer().then(val => { jwtUserConfirmer = val; }).catch(rej => { console.log("794", rej); });}, 172800); // Refresh jwt tokens every 2 days
	});  

var httpServer = http.createServer(app)
	.listen(8080, () => {
		console.log("http listening on 8080");
	});