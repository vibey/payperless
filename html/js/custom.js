"use strict";

jQuery(document).ready(function () {

	if(getCookie('pp-cookie-accepted') == "") {
		jQuery('.root').append(`
		<div class="cookie-bar">
			<div class="container">
				<div class="cookie-text">
					<p>Diese Website verwendet Cookies. Indem Du diese Website nutzt, erklärst Du Dich mit dieser Verwendung einverstanden.&nbsp;<a target="_blank" href="./startprivacy">Weitere Informationen</a><i class="fal fa-times" id="cookie-agree"></i></p>
				</div>
			</div>
		</div> 
		`);
		jQuery('#cookie-agree').on('click', function() {
			setCookie('pp-cookie-accepted', 'accepted', 365);
			jQuery('.root').addClass('cookie-set');
			jQuery('.cookie-bar').hide();
		});
	} 

	function getQueryVariable(variable)
	{
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if(pair[0] == variable){return pair[1];}
		}
		return(false);
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	function getCookie(cname) {
		var name = cname + "="; 
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	function showAddToHomescreen() {
		let ppbtn = document.querySelectorAll('#pwaprompt');
		//ppbtn
	}

	let recaptchaToken = "";
	if(jQuery('html.login, html.register, html.cardregister, html.linkcard, html.contact, html.profile').length) {
		grecaptcha.ready(function() {
			grecaptcha.execute('6LcRYMwUAAAAADpsnmTkt8eKvr6IbCuHsk-JIPdT', {action: 'authenticate'}).then(function(token) {
				recaptchaToken = token;
			},rej => {
				console.log("Recaptcha error", rej);
			});
		});
	}

	let deferredPrompt;
	
	/* Check if Website is installed as standalone PWA */
	const isInStandaloneMode = () => (window.matchMedia('(display-mode: standalone)').matches) || (window.navigator.standalone) || document.referrer.includes('android-app://');

	if (isInStandaloneMode()) {

		if(window.location.href == "https://payper-less.com/index.html" && navigator.onLine === true) {
			window.location = 'https://payper-less.com';
		} else {

		}

		jQuery('a[target="_blank"]').each(function() {
			jQuery(this).attr('target', '_self');
		});
		
		if(!navigator.onLine) {
			// If the PWA is opened and user is in offline state
		}
	} else {
		 
		window.addEventListener('beforeinstallprompt', (e) => {
			//e.preventDefault(); // prevent minibar to popup
			// Stash the event so it can be triggered later.
			deferredPrompt = e; 
			let pwabtn_menu = document.querySelector('#pwaprompt-menu');
			let pwaform = document.querySelector('.pwa-menu-form');
			jQuery(pwaform).show();
			jQuery(pwabtn_menu).on('click', function(e) {
				deferredPrompt.prompt();
			});
			// Update UI notify the user they can add to home screen
			//showInstallPromotion();
			//showAddToHomescreen();
		});

	}

	let reCaptcha = {};

	if(jQuery(window).width() > 575) {
		jQuery(window).scroll(() => {
			if(jQuery(window).scrollTop() > jQuery('.navbar').outerHeight()) {
				jQuery('.navbar').addClass('scrolled');
			} else if(jQuery('.navbar').hasClass('scrolled')){
				jQuery('.navbar').removeClass('scrolled');
			}
		});
	} else {
		jQuery(window).scroll(() => {
			if(jQuery(window).scrollTop() > 0) {
				jQuery('.navbar').addClass('scrolled');
			} else if(jQuery('.navbar').hasClass('scrolled')){
				jQuery('.navbar').removeClass('scrolled');
			}
		});
	}
	/*
	if(jQuery('.navbar-toggler').length) {
		jQuery('.navbar-toggler').on('click', (e) => {
			if(!jQuery('.animated-icon2').hasClass('open')) {
				if(!jQuery('.root').hasClass('nav-show')) {
					jQuery('.root').addClass('nav-show');
				}
				jQuery('.animated-icon2').addClass('open');
			} else {
				if(jQuery('.root').hasClass('nav-show')) {
					jQuery('.root').removeClass('nav-show');
				}
				jQuery('.animated-icon2').removeClass('open');
			}
		});
	} */

	function setInputFilter(textbox, inputFilter) {
		["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
		  textbox.addEventListener(event, function() {
			if (inputFilter(this.value)) {
			  this.oldValue = this.value;
			  this.oldSelectionStart = this.selectionStart;
			  this.oldSelectionEnd = this.selectionEnd;
			} else if (this.hasOwnProperty("oldValue")) {
			  this.value = this.oldValue;
			  this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			} else {
			  this.value = "";
			}
		  });
		});
	  }

	/*if(document.querySelectorAll('html.registration').length > 0) {
		let registerObj = {};
		
		let regBtn1 = document.querySelector('#regBtn1');
		
		let regBtn2 = document.querySelector('#regBtn2');

		regBtn1.addEventListener('click', function() {
			
			let uname = document.querySelector('#uname');
			uname = uname.value;
			let pword = document.querySelector('#pword');
			pword = pword.value;
			if(uname != "" && pword != "") {

				registerObj.username = uname;
				registerObj.password = pword;
				jQuery.post('/beginreg', registerObj, (res) => {
			
					if(res.email_error) {
						jQuery('#uname + small').remove();
						jQuery('#uname').after("<small class='input-error'>Email not valid or already exsists!</small>");
					}
					if(res.password_error) {
						jQuery('#pword + small').remove();
						jQuery('#pword').after("<small class='input-error'>Password doesn't fit the requestet complexity!</small>");
					}
					if(!res.password_error && !res.email_error && res.validLogin) { 
						jQuery('.registration-wrap').hide();
						jQuery('.registration-wrap.nickname').show();
						regBtn2.addEventListener('click', (e) => {
							let unickname = document.querySelector('#unickname');
							if(unickname.value != "") {
								registerObj.nick = unickname.value;
								jQuery.post('/completereg', registerObj, (res) => {
									console.log(res);
								});
							}

						});
					}
				}); 
			}

			//let nicknameForm = document.querySelector('.registration-wrap.nickname');
			//nicknameForm.style.display = "block";
		});
	}*/

	if(document.querySelectorAll('html.cardregister').length == 1) {
		setInputFilter(document.getElementById('cardnumber'), function(value) { return /^\d*$/.test(value); });
		jQuery('#confirmNumber').hide();
		jQuery('#cardnumber').focus();
		jQuery('#cardnumber').on('keyup', (e) => {
			if(jQuery(e.target).val().length == 9) {
				jQuery('#confirmNumber').show();
			} else {
				jQuery('#confirmNumber').hide();
			}
		});

		jQuery('#confirmNumber').on('click', function(e) {
			let confirmObj = {};
			confirmObj.cardnumber = jQuery('#cardnumber').val();
			confirmObj.token = recaptchaToken;
			jQuery('#cardnotfounderror').remove();

			jQuery.post('/begincardreg', confirmObj, (res) => { 

				if(typeof res === 'object') {
					if('notfound_error' in res) {
						if(res.notfound_error) {
							jQuery('#cardnotfounderror').remove();
							jQuery('#confirmNumber').after("<p class='mt-0 mb-0' id='cardnotfounderror'><small class='text-danger'>Die angegebene Kartennummer ist ungültig.</small></p>");
						}
					}
				} else {
					jQuery('.root > .container').html(res);
			
					if(jQuery('.cardregister-form.step-2').length) {
						jQuery('#cmail').focus();
						jQuery('#confirmEmail').on('click', (e) => {
							jQuery('#emailareadyusederror').remove();
							if(validator.isEmail(jQuery('#cmail').val())) {
								confirmObj.umail = jQuery('#cmail').val();
								jQuery.post('/successcardreg', confirmObj, (res) => {
									if(typeof res === 'object') {
										if('part_error' in res) {
											alert("Es scheint ein Fehler beim Laden der Seite aufgetreten zu sein. Bitte laden Sie die Seite erneut.");
										} else if('error_message' in res) {
											alert(res.error_message);
										} else if('email_error' in res) {
											if(res.email_error) {
												jQuery('#cmail').after("<p class='mt-0 mb-0' id='emailareadyusederror'><small class='text-danger'>Bitte geben Sie eine gültige E-Mail ein.</small></p>");
											}
										}
									} else {
										jQuery('.root > .container').html(res);
									}
								});
							} else { 
								jQuery('#cmail').after("<p class='mt-0 mb-0' id='emailareadyusederror'><small class='text-danger'>Bitte geben Sie eine gültige E-Mail ein.</small></p>");
							}
						});
					}
				}

			});
		});

		jQuery('#cardnumber').on('keydown', function(e) {
			if(e.keyCode == 13) {
				if(jQuery('#confirmNumber').is(':visible')) {
					jQuery('#confirmNumber').trigger('click');
				}
			}
		});
	}

	if(jQuery('html.linkcard').length) {
		setInputFilter(document.getElementById('cardnumber'), function(value) { return /^\d*$/.test(value); });
		jQuery('#connectCard').hide();
		jQuery('#cardnumber').focus();
		jQuery('#cardnumber').on('keyup', (e) => {
			if(jQuery(e.target).val().length == 9) {
				jQuery('#connectCard').show();
			} else {
				jQuery('#connectCard').hide();
			}
		});

		jQuery('#connectCard').on('click', function(e) {
			jQuery('#cardnotfounderror').remove();
			jQuery('#cardused').remove();
			jQuery('#cardconnected').remove();

			let linkcardObj = {
				cardnumber: jQuery('#cardnumber').val()
			};

			jQuery.post('/linkcard', linkcardObj, res => {
				if('success' in res) {
					jQuery('label[for="cardnumber"]').remove();
					jQuery('#connectCard').after("<p class='mt-0 mb-0' id='cardconnected'><small class='text-success'>Die Karte wurde erfolgreich verknüpft!</small></p>");
				}
				if('notfound_error' in res) {
					if(res.notfound_error) {
						jQuery('#connectCard').after("<p class='mt-0 mb-0' id='cardnotfounderror'><small class='text-danger'>Die angegebene Kartennummer ist ungültig.</small></p>");
					}
				} else if('already_used' in res) {
					if(res.already_used) {
						jQuery('#connectCard').after("<p class='mt-0 mb-0' id='cardused'><small class='text-danger'>Die Karte ist bereits mit einem Konto verknüpft.</small></p>");
					}
				}
			});
		});

		jQuery('#cardnumber').on('keydown', function(e) {
			if(e.keyCode == 13) {
				if(jQuery('#connectCard').is(':visible')) {
					jQuery('#connectCard').trigger('click');
				}
			}
		});
	}

	jQuery('a[href^="#"], .home a[href^="/#"]').on('click',function(e) {
		e.preventDefault();
		var target = this.hash;
		var thisTarget = jQuery(target);
		$('html, body').stop().animate({
		 'scrollTop': (thisTarget.offset().top - 60)
		}, 900, 'swing');
	 });

	/*var treeCounter = new FlipClock(jQuery('.tree-counter-flipclock'), {
		clockFace: 'Counter',
		minimumDigits: 6,
		running: false, 
		countdown: false
	});*/

	let counter = 0;

	/*setInterval(() => {
		counter++;
		treeCounter.setTime(counter);
	}, 1000);*/

	if(jQuery('[data-toggle="tooltip"]').length) {
		jQuery('[data-toggle="tooltip"]').tooltip();
	}	

	if(jQuery('html.register').length) {
		// <p class='mt-0 mb-0' id='emailerror'><small class='text-danger'>Bitte geben Sie eine gültige E-Mail ein.</small></p>
		
		jQuery('#confirmRegistration').on('click', function(e) {
			jQuery(this).html('<img src="/img/loading.gif">');
			let uemail = jQuery('#uemail'); 
			let password = jQuery('#password');
			let passwordrepeat = jQuery('#passwordrepeat');

			jQuery('.emailerror').remove();
			jQuery('.passworderror').remove();
			jQuery('.emailsenderror').remove();

			if(!validator.isEmail(uemail.val()) || (password.val() != passwordrepeat.val()) || password.val() == "" || passwordrepeat.val() == "") {
				jQuery(this).html('Registrieren');
				if(!validator.isEmail(uemail.val())) {
					uemail.after("<p class='mt-0 mb-0 emailerror'><small class='text-danger'>Bitte geben Sie eine gültige E-Mail ein.</small></p>");
				}
				if(password.val() == "") {
					password.after("<p class='mt-0 mb-0 passworderror'><small class='text-danger'>Bitte geben Sie ein Passwort ein.</small></p>");
				}
				if(passwordrepeat.val() == "") {
					passwordrepeat.after("<p class='mt-0 mb-0 passworderror'><small class='text-danger'>Bitte geben Sie ein Passwort ein.</small></p>");
				}
			} else { 
				let registerObj = {
					email: uemail.val(),
					password: password.val(),
					token: recaptchaToken
				}
				jQuery.post('/register', registerObj, res => {
					if(typeof res === 'object') {
						jQuery(this).html('Registrieren');
						if(res.password_error) {
							password.after("<p class='mt-0 mb-0 passworderror'><small class='text-danger'>Bitte geben Sie ein sicheres Passwort ein.</small></p>");
							passwordrepeat.after("<p class='mt-0 mb-0 passworderror'><small class='text-danger'>Bitte geben Sie ein sicheres Passwort ein.</small></p>");
						}
						if(res.email_error) {
							uemail.after("<p class='mt-0 mb-0' id='emailerror'><small class='text-danger'>Bitte geben Sie eine gültige E-Mail ein.</small></p>");
						}
						if(res.cantsendconfirmation_error) {
							jQuery('.card-middle').append("<p class='mt-0 mb-0 emailsenderror'><small class='text-danger'>Wir konnten keine Aktivierungsmail an Ihre angegebene E-Mail-Adresse versenden.</small></p>");
						}
					} else { 
						jQuery('html.register .root > .container').html(res);
					}
				});
			}
		});
		jQuery('#passwordrepeat, #password, #uemail').on('keyup', function(e) {
			if(e.keyCode === 13) {
				jQuery('#confirmRegistration').trigger('click');
			}
		});
	}

	if(jQuery('html.login').length) {
		jQuery('#login').on('click', function(e) {
			jQuery('.loginerror').remove();
			jQuery('.emptydataerror').remove();
			let inputEmail = jQuery('#uemail');
			let inputPassword = jQuery('#password');
			let loginObj = {
				email: inputEmail.val(),
				password: inputPassword.val(),
				token: recaptchaToken
			}

			if(jQuery('#uemail').val() != "" && jQuery('#password') != "") {
				jQuery(this).html('<img src="/img/loading.gif">');
				jQuery.post('/login', loginObj, res => {
					if(typeof res === 'object') {
						if('login_error' in res) {
							if('message' in res) {
								if(res.message === "Auth.form.error.invalid") {
									jQuery('#password').after("<p class='mt-0 mb-0 loginerror'><small class='text-danger'>Die Logindaten stimmen nicht überein.</small></p>");
								} else {
									jQuery('#password').after("<p class='mt-0 mb-0 loginerror'><small class='text-danger'>Es ist ein Fehler beim Anmelden aufgetreten. Versuchen Sie es später erneut.</small></p>");
								}
							}
							jQuery('#login').html('Anmelden');
						} else {
							let redirect = res.redirect;
							let uData = res.uData;
							window.localStorage.setItem('uData', JSON.stringify(uData));
							window.location.href = redirect;
						} 
					} else {
						jQuery('#login').html('Anmelden');
					}
				});
			} else {
				if(jQuery('#uemail').val() == "") {
					jQuery('#uemail').after("<p class='mt-0 mb-0 emptydataerror'><small class='text-danger'>Bitte geben Sie Ihre E-Mail-Adresse ein.</small></p>");
				}

				if(jQuery('#password').val() == "") {
					jQuery('#password').after("<p class='mt-0 mb-0 emptydataerror'><small class='text-danger'>Bitte geben Sie Ihr Passwort ein.</small></p>");
				}
			}
		});

		jQuery('#uemail').focus();

		jQuery('#uemail, #password').on('keydown', function(e) {
			if(e.keyCode == 13) {
				jQuery('#login').trigger('click');
			}
		})
	}

	/* Sidemenu togglers */
	jQuery('.navbar-toggler-page').on('click', function(e) {
		let self = jQuery(this);
		let bars = jQuery('.animated-icon2', this);
		if(!jQuery('.root.nav-show').length) {
			jQuery('.root').addClass('nav-show');
			jQuery('.root').append('<div class="side-menu-overlay"></div>');
		}
	});

	jQuery('.navbar-toggler-side').on('click', function(e) {
		let self = jQuery(this);
		let bars = jQuery('.animated-icon2', this);
		if(jQuery('.root.nav-show').length) {
			jQuery('.root').removeClass('nav-show');
			jQuery('.side-menu-overlay').remove(); 
		}
	}); 

	jQuery(document).on('click', '.side-menu-overlay', function(e) {
		if(jQuery('.root.nav-show').length) {
			jQuery('.root').removeClass('nav-show');
		}
	});

	if(jQuery('html.barcode').length) {
		if(jQuery('#barcode').length) {
			if(localStorage.getItem('uData')) {
				let uData = JSON.parse(localStorage.getItem('uData'));
				if('cardnumberFull' in uData) {
					JsBarcode('#barcode')
						.EAN13(uData.cardnumberFull, {fontSize: 18, textMargin: 0, height: 50, textAlign: "center", marginTop: 25})
						.blank(20)
						.render();
				} else {
					jQuery('#barcode').after('<p class="pp-hint">Ihr Barcode scheint zu fehlen! Bitte erneut einloggen.</p>');
				}
			}
		}
	}

	if(jQuery('html.home').length) {
		if(!navigator.onLine) {
			jQuery('#recaptchaScript').remove();
			let offlineHTML = `
				<div class="container d-flex justify-content-center align-items-center" style="height: calc(100vh - 60px); width: 100%;">
					<svg class="my-barcode-svg" id="barcode"></svg>
				</div>
				<style>
					p { display: none !important; }
				</style>
			`;
			jQuery('.root').html(offlineHTML);
			setTimeout(() => {
				jQuery('#recaptchaScript').remove();
				if(jQuery('#barcode').length) {
					if(localStorage.getItem('uData')) {
						let uData = JSON.parse(localStorage.getItem('uData'));
						if('cardnumberFull' in uData) {
							JsBarcode('#barcode')
								.EAN13(uData.cardnumberFull, {fontSize: 18, textMargin: 0, height: 50, textAlign: "center", marginTop: 25})
								.blank(20)
								.render();
						} else {
							jQuery('#barcode').after('<p class="pp-hint">Ihr Barcode scheint zu fehlen! Bitte online gehen und einloggen!</p>');
						}
					}
				}
			}, 1000); 
			
		}
	}

	if(jQuery('html.profile').length) {
		jQuery('#submit-email-change').on('click', function(e) {
			let newEmail = jQuery('#new-email');
			let password = jQuery('#confirm-email-password');
			let mailchangeObj = {
				email: newEmail.val(),
				password: password.val(),
				token: recaptchaToken 
			};
			jQuery.post('/changemail', mailchangeObj, res => {
				jQuery('.emailchangemessage').remove();
				if(typeof res === 'object') {
					if('emailchange_error' in res) {
						if(!res.emailchange_error) {
							jQuery('#email-change-form').after("<p class='mt-2 mb-0 emailchangemessage'><small class='text-success'>Deine E-Mail-Adresse wurde erfolgreich geändert.</small></p>");
							jQuery('#current-email').val(newEmail.val());
						} else {
							jQuery('#email-change-form').after("<p class='mt-2 mb-0 emailchangemessage'><small class='text-danger'>Bitte geben Sie Ihr Passwort ein.</small></p>");
						}
					}
				}
			});

		});
		if(jQuery('#hasProvider').length) {
			let provider = jQuery('#hasProvider').html();
			jQuery('#email-change-form').html("<p class='email-change-disabled'>Diese Funktion steht nicht zur Verfügung, da Du dich mit <strong class='text-success'>" + provider + "</strong> eingeloggt hast.</p>");
			jQuery('#password-change-form').html("<p class='email-change-disabled'>Diese Funktion steht nicht zur Verfügung, da Du dich mit <strong class='text-success'>" + provider + "</strong> eingeloggt hast.</p>");
		}
	}

	jQuery('.prompt-app-download').on('click', function(e) {
		deferredPrompt.prompt();
	});

	// Receipts filter

	let purchaseDate = document.querySelector('#date-filter-toggle');
	let findShop = document.querySelector('#shop-filter-toggle');
	let findProduct = document.querySelector('#product-filter-toggle');
	let purchaseDateFilter = document.querySelector('.date-filters');
	let findShopFilter = document.querySelector('.shop-filter');
	let findProductFilter = document.querySelector('.product-filter');

	$(purchaseDate).click(() => {
		if(!$(purchaseDateFilter).is(':visible')) {
			$(purchaseDateFilter).show();
		} else {
			$(purchaseDateFilter).hide();
		}
		if($(findShopFilter).is(':visible')) {
			$(findShopFilter).hide();
		}
		if($(findProductFilter).is(':visible')) {
			$(findProductFilter).hide();
		}
	});

	$(findShop).click(() => {
		if(!$(findShopFilter).is(':visible')) {
			$(findShopFilter).show();
		} else {
			$(findShopFilter).hide();
		}
		if($(purchaseDateFilter).is(':visible')) {
			$(purchaseDateFilter).hide();
		}
		if($(findProductFilter).is(':visible')) {
			$(findProductFilter).hide();
		}
	});

	$(findProduct).click(() => {
		if(!$(findProductFilter).is(':visible')) {
			$(findProductFilter).show();
		} else {
			$(findProductFilter).hide();
		}
		if($(purchaseDateFilter).is(':visible')) {
			$(purchaseDateFilter).hide();
		}
		if($(findShopFilter).is(':visible')) {
			$(findShopFilter).hide();
		}
	});

	if(jQuery('html.contact').length) {
		if(jQuery('#privacy-checkbox').is(':checked')) {
			jQuery('#contact-submit').removeAttr('disabled');
		} else {
			jQuery('#contact-submit').attr('disabled', true);
		}

		jQuery('label[for="privacy-checkbox"]').on('click', function(e) {
			if(jQuery('#privacy-checkbox').is(':checked')) {
				jQuery('#contact-submit').removeAttr('disabled');
			} else {
				jQuery('#contact-submit').attr('disabled', true);
			}
		});

		jQuery('#contact-submit').on('click', function(e) {
			jQuery('.text-success').remove();
			jQuery('.text-danger').remove();
			if(jQuery('#privacy-checkbox').is(':checked')) {
				let name = jQuery('#contact-form #contactname');
				let email = jQuery('#contact-form #contactmail');
				let message = jQuery('#contact-form #contactmessage');

				let messageObj = {
					name: name.val(),
					email: email.val(),
					message: message.val(),
					token: recaptchaToken
				}

				if(name.val() != "" && email.val() != "" && message.val() != "") {
					jQuery.post('/contact', messageObj, res => {
						if(res.send_success) {
							jQuery('#contact-form').after("<p class='text-success mt-2'>Ihre Kontaktanfrage wurde versendet!</p>");
						} else {
							jQuery('#contact-form').after("<p class='text-danger mt-2'>Fehler beim Versenden Ihrer Kontaktanfrage!</p>");
						}
					});
				} else {
					if(name.val() == "") {
						jQuery(name).after("<p class='text-danger'>Bitte geben Sie Ihren Namen ein</p>");
					} 
					if(email.val() == "") {
						jQuery(email).after("<p class='text-danger'>Bitte geben Sie Ihre E-Mail-Adresse ein</p>");
					}
					if(message.val() == "") {
						jQuery(message).after("<p class='text-danger'>Bitte geben Sie eine Nachricht ein</p>");
					}
				}	
			}
		});
	}

	if(jQuery('html.home, html.start').length) {

		if(jQuery('#dsgvo-checkbox').is(':checked')) {
			jQuery('#support-submit').removeAttr('disabled');
		} else {
			jQuery('#support-submit').attr('disabled', true);
		}

		jQuery('label[for="dsgvo-checkbox"]').on('click', function(e) {
			if(jQuery('#dsgvo-checkbox').is(':checked')) {
				if(validator.isEmail(jQuery('#support-email').val())) {
					jQuery('#support-submit').removeAttr('disabled');
				}
			} else {
				jQuery('#support-submit').attr('disabled', true);
			}
		});

		jQuery('#support-email').on('keyup', function(e) {
			if(validator.isEmail(jQuery('#support-email').val()) && jQuery('#dsgvo-checkbox').is(':checked')) {
				jQuery('#support-submit').removeAttr('disabled');
			} else {
				jQuery('#support-submit').attr('disabled', true);
			}	
		});

		jQuery('#support-submit').on('click', function(e) {
			let notification = false;
			if(validator.isEmail(jQuery('#support-email').val()) && jQuery('#dsgvo-checkbox').is(':checked')) {
				if(jQuery('#support-checkbox').is(':checked')) {
					notification = true;
				}
				let email = jQuery('#support-email').val();
				let supporter = {
					email: email,
					notification: notification
				};
				
				jQuery.post('/supporter', supporter, res => {
					if('already_exists' in res) {
						jQuery('#support-submit').after("<p class='text-danger mt-2'>Du unterstützt uns bereits!</p>");
					} else if('success' in res) {
						jQuery('#support-submit').replaceWith("<p class='text-success mt-3'>Vielen Dank für Deine Unterstützung!</p>");
						jQuery('#support-counter').html((parseInt(jQuery('#support-counter').html()) + 1));
					}
				});
			}
		});
	}
	
	if(jQuery('html.receipt').length) {
		jQuery('#product').on('keyup', function(e) {
			let searchString = jQuery(this).val();
		
			if(e.keyCode == 13) {
				console.log("search");
				let searchObj = {
					type: 'product',
					search: searchString
				};
				jQuery.get('/search', searchObj, res => {
					console.log(res);
				});

			}
		})
	}

	if(jQuery('html.reset').length) {
		let password = jQuery('#password');
		let passwordRepeat = jQuery('#passwordrepeat');
		jQuery('#confirmReset').on('click', function() {
			jQuery('.passworderror').remove();
			if(password.val() === "") {
				password.after("<p class='text-danger passworderror'>Bitte gib ein neues Password ein</p>");
			} 
			if(passwordRepeat.val() === "") {
				passwordRepeat.after("<p class='text-danger passworderror mb-0'>Bitte wiederhole Dein Passwort</p>");
			} 

			if(password.val() !== "" && passwordRepeat.val() !== "") {
				if(password.val() === passwordRepeat.val()) {
					jQuery.post('/reset', {
						password: password.val(),
						passwordrepeat: passwordRepeat.val() 
					}, res => {
						if("success" in res) {
							passwordRepeat.after("<p class='text-success passworderror'>Dein Passwort wurde erfolgreich geändert</p>");
						} else if("passwordchangeerror" in res) {
							passwordRepeat.after("<p class='text-danger passworderror'>Es ist ein Fehler aufgetreten bei dem Versuch Dein Passwort zu ändern. Bitte versuche es später erneut.</p>");
						} else if("passwordcomplexityerror" in res) {
							passwordRepeat.after("<p class='text-danger passworderror'>Passwort entspricht nicht den Sicherheitsanforderungen</p>");
						} else if("identicerror" in res) {
							passwordRepeat.after("<p class='text-danger passworderror'>Passwörter stimmen nicht überein</p>");
						}
					});
				} else {
					passwordRepeat.after("<p class='text-danger passworderror'>Passwörter stimmen nicht überein</p>");
				}
			}
		});
	}

	if(jQuery('html.profile').length) {
		jQuery('#reset-password').on('click', function() {
			jQuery.post('/forgot', {}, (res) => {});
			setTimeout(function() { window.location.href = "https://payper-less.com/forgot"; }, 1000);
		});
	}

	if(window.location.href.indexOf("?cf=") > -1) {
		let storageObj = {
			cardnumber: getQueryVariable('c'),
			cardnumberFull: getQueryVariable('cf'),
			cardnumberId: getQueryVariable('id')
		};
		console.log(storageObj);

		window.localStorage.setItem('uData', JSON.stringify(storageObj));
	}

});

jQuery(window).on('load', () => {
	if(jQuery(window).width() > 575) {
		jQuery('.row').each(function() {
			let heigehst = 0;
			jQuery('div[class*="col-"].fit-height', this).each(function() {
				if(jQuery(this).height() > heigehst) {
					heigehst = jQuery(this).height();
				}
			})

			jQuery('div[class*="col-"].fit-height', this).each(function() {
				jQuery(this).height(heigehst);
			})
		});
	}

	if(jQuery(window).width() > 575) {
		if(jQuery(window).scrollTop() > jQuery('.navbar').outerHeight()) {
			jQuery('.navbar').addClass('scrolled');
		} else if(jQuery('.navbar').hasClass('scrolled')){
			jQuery('.navbar').removeClass('scrolled');
		}
	} else {
		if(jQuery(window).scrollTop() > 0) {
			jQuery('.navbar').addClass('scrolled');
		} else if(jQuery('.navbar').hasClass('scrolled')){
			jQuery('.navbar').removeClass('scrolled');
		}
	}

	/* Service Worker */
	let swRegistration;

	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.register('/service-worker.js')
			.then(function(swReg) {
				swRegistration = swReg;
			}).catch(function(error) {
				console.error('Service Worker Error', error);
			});
	}

	if(window.Notification) {

	}

	if('PushManager' in window) { 
		// Show App install buttons / banners etc...
		/*
		let permission = window.Notification.requestPermission().then(res => {
		}).catch(rej => {
			console.log("Notification error", rej);
		});*/

	}

	if(jQuery('html.home, html.start').length) {
		jQuery.get('/supporter', res => {
			jQuery('#support-counter').html(res.count);
			/*jQuery('#support-counter').counterUp({
				delay: 10, 
				time: 1000
			});*/
		});
	}
});